#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "monopoly.h"

static player_t create_player(char *name)
{
    player_t new_player = {FREE, strdup(name), 1500, 0};

    return new_player;
}
bool player_exist(player_t *players, size_t player_nbr, char *name)
{
    for (size_t i = 0; i < player_nbr; i++)
        if (!strcmp(players[i].name, name))
            return false;
    return true;
}
void player_list(player_t *players, size_t player_nbr)
{
    printf("Liste des dresseurs (%ld):\n", player_nbr);
    for (size_t i = 0; i < player_nbr; i++)
        printf("- %s", players[i].name);
}
player_t *add_player(player_t *players, size_t *player_nbr)
{
    char *name = NULL;

    printf("Choisissez votre nom où écrivez \"MENU\": \n");
    for (size_t i = 0; getline(&name, &i, stdin) != -1 && strcmp(name, "MENU\n");)
    {
        if (player_exist(players, *player_nbr, name))
        {
            players = reallocarray(players, *player_nbr + 1, sizeof(player_t));
            players[*player_nbr] = create_player(name);
            *player_nbr += 1;
        }
        else
            printf("Nom déjà utilisé.\n");
        printf("Choisissez votre nom où écrivez \"MENU\": \n");
        free(name);
        name = NULL;
    }
    free(name);
    return players;
}