#include <stddef.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "dice.h"
#include "monopoly.h"
#include "print.h"

// fonction qui deplace le joueur
int move(player_t *player, int first_dice, int secound_dice)
{
    int sum = first_dice + secound_dice;
    int i = player->position;

    // verifie si le joueur n'est pas en prison ou si il a fait un double pour sortir de prison
    if (player->status != JAIL || (first_dice == 6 && secound_dice == 6))
    {
        // cette boucle deplace le joueur dans la carte et si il passe par la case depart lui ajoute 200 pokedollard
        for (i = player->position; sum != 0; sum--, i + 1 > 39 ? i = 0, player->money += 200 : i++)
            ;
        player->position = i;
        player->status = FREE;
        return (0);
    }
    printf("Vous restez en prison\n");
    return (1);
}
void buy_propertie(player_t *player, card_t *card, bank_t *bank)
{
    char *input = NULL;
    size_t size = 0;
    int selection = 0;

    // verifie si la carte est une ville
    if (card->type == CITY)
    {
        // verifie si la carte n'a pas de proprietaire
        if (card->owner == NULL)
        {
            printf("Acheter ce pokemon pour: %d pokedollard? (OUI/NON)\n", card->price[BASE]);
            getline(&input, &size, stdin);
            if (!strcmp(input, "OUI\n"))
            {
                if (player->money - card->price[BASE] >= 0)
                {
                    card->owner = player;
                    player->money -= card->price[BASE];
                }
                else if (player->money - card->price[BASE] < 0)
                    printf("Vous n'avez pas suffisament d'argent\n");
            }
        }
        // verifie si le proprietaire est egalement le joueur
        else if (card->owner != NULL && !strcmp(player->name, card->owner->name))
        {
            printf("Il y a %ld attaques et %ld objets sur ce pokemon, que voulez-vous rajouter ? (1 : attaques, 2 : objets)\n", card->move, card->item);
            getline(&input, &size, stdin);
            selection = atoi(input);
            if (selection == 1)
            {
                printf("Vous avez %ld attaques, combien voulez vous en ajouter ? (MAX 4, 50 pokedollard/attaques)\n", card->move);
                while (getline(&input, &size, stdin) != -1)
                {
                    selection = atoi(input);
                    free(input);
                    input = NULL;
                    // verifie si le joueur à les moyen de payer
                    if (selection + card->move <= 4 && player->money - (selection * 50) >= 0 && (int)(bank->move - selection) >= 0)
                    {
                        bank->move -= 1;
                        player->money -= selection * 50;
                        card->move += selection;
                        break;
                    }
                    printf("Vous avez %ld attaques, combien voulez vous en ajouter ? (MAX 4, 50 pokedollard/attaques)\n", card->move);
                }
            }
        }
        else
            printf("Ce pokémon ne vous appartient pas\n");
    }
    free(input);
}
// Recuperer le prix a payer quand on tombe sur une carte
int rent(card_t card)
{
    if (card.item != 0)
        return (card.price[HOTEL]);
    switch (card.move)
    {
    case 0:
        return (card.price[RENT]);
    case 1:
        return (card.price[FIRST_HOUSE]);
    case 2:
        return (card.price[SECOUND_HOUSE]);

    case 3:
        return (card.price[THIRD_HOUSE]);
    case 4:
        return (card.price[FOURTH_HOUSE]);
    default:
        return 0;
    }
    return 0;
}
// applique des effet au joueur lorsqu'il tombe sur une case
int apply_map_effect(card_t card, player_t *player)
{
    int luck = rand() % 2;

    switch (card.type)
    {
    case CITY:
        if (card.owner)
        {
            card.owner->money += rent(card);
            player->money -= rent(card);
        }
        break;
    case LUCK:
        if (luck)
        {
            printf("C'est ton anniversaire vous gagné 200 pokédollar\n");
            player->money += 200;
        }
        else
        {
            printf("Vous perdez un combat, vous payez 200 pokédollar\n");
            player->money -= 200;
        }
        break;
    case TAX:
        printf("Vous perdez un combat, vous payez 200 pokédollar\n");
        player->money -= 200;
        break;
    case GOTO_PRISON:
        printf("Vous avez été capturé par la team Galaxy, vous allez en prison\n");
        player->position = 10;
        player->status = JAIL;
        break;
    default:
        break;
    }
    // regarde si le joueur n'a plus d'argent
    if (player->money < 0)
    {
        printf("Le dresseur %s à perdu\n", player->name);
        return 1;
    }
    else
        return 0;
}
// lance la boucle de jeu
void game_loop(player_t *players, size_t player_nbr, card_t *map)
{
    bank_t bank = {32, 12, INT_MAX};
    char *input = NULL;
    size_t size = 0;
    int action = 0;
    int first_dice = 0;
    int secound_dice = 0;
    bool moved = false;

    // determine en aleatoire le joueur qui joue et fait une boucle pour passer sur tout les joueurs a l'infini
    for (size_t i = coin_flip_start(player_nbr); 1; i >= player_nbr - 1 ? i = 0 : i++)
    {
        print_map();
        print_players_stats(map, players, player_nbr);
        printf("C'est le tour du dresseur %s", players[i].name);
        print_game_option();

        while (getline(&input, &size, stdin) != -1)
        {
            action = atoi(input);
            free(input);
            input = NULL;
            if (action == 1)
            {
                // empeche le joueur de lancer les dé plus d'une fois
                if (moved == false)
                {
                    first_dice = throw_dice();
                    secound_dice = throw_dice();
                    printf("vous avez fait %d, %d\n", first_dice, secound_dice);
                    move(&(players[i]), first_dice, secound_dice);
                    if (apply_map_effect(map[players[i].position], &(players[i])) == 1)
                        return;
                    print_players_stats(map, players, player_nbr);
                    moved = true;
                }
                else
                    printf("Vous ne pouvez plus vous deplacer pendant ce tour\n");
            }
            else if (action == 2)
                buy_propertie(&(players[i]), &(map[players[i].position]), &bank);
            else if (action == 3)
                print_properties(map, players, player_nbr);
            else if (action == 4)
            {
                moved = false;
                break;
            }
            else if (action == 5)
                return;
            print_game_option();
        }
    }
}