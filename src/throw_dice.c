#include <stdlib.h>

int throw_dice(void)
{
    return (rand() % 6) + 1;
}

int coin_flip_start(size_t player_nbr)
{
    return (rand() % player_nbr);
}