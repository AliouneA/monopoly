#include <stddef.h>
#include <stdlib.h>

#include "monopoly.h"

void destroy_players(player_t **players, size_t player_nbr)
{
    for (size_t i = 0; i < player_nbr; i++)
        free((*players)[i].name);
    free(*players);
}