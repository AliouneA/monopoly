#include <stdlib.h>
#include <time.h>

#include "monopoly.h"
#include "print.h"
#include "utils.h"

int main(void)
{
    // permet de generer une nouvelle seed pour avoir des chiffres random differents a chaque lancement du programme
    srand(time(NULL));
    print_title();
    print_menu();
    return (0);
}