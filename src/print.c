#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "monopoly.h"
#include "colors.h"
#include "player.h"
#include "utils.h"

void print_map(void)
{
    printf("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
    printf("|DEPART| |kranidos|  |Caisse de communauté|  |Onix|  |Vous perdez un combat|  |Centre pokemon NORD|  |Ceriflor|  |Chance|  |Tortipouss| |Roserade| |Prison Team Galaxy|\n");
    printf("|Cochignon|                                                                                                                                                 |Méditikka|\n");
    printf("|Blizzaroi|                                                                                                                                     |Vous perdez un combat|\n");
    printf("|Caisse de communauté|                                                                                                                                      |Machopeur|\n");
    printf("|Momartik|                                                                                                                                                    |Lucario|\n");
    printf("|Centre pokemon OUEST|                                                                                                                             |Centre pokemon EST|\n");
    printf("|Chance|                                                                                                                                                     |Léviator|\n");
    printf("|Luxray|                                                                                                                                         |Caisse de communauté|\n");
    printf("|Vous perdez un combat|                                                                                                                                      |Maraiste|\n");
    printf("|Élekable|                                                                                                                                                 |Mustéflott|\n");
    printf("|Aire de détente| |Grodrive|Chance| |Ectoplasma| |Magirêve| |Centre pokemon SUD| |Magnéton| |Steelix| |Vous perdez un combat| |Bastiodon| |Capturer par la team Galaxy|\n");
    printf("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
}
void print_players_stats(card_t *map, player_t *players, size_t player_nbr)
{
    for (size_t i = 0; i < player_nbr; i++)
    {
        printf("Dresseur %s \t - Actuellement à %s\n", players[i].name, map[players[i].position].name);
        printf("\t - Pokédollar %d\n", players[i].money);
    }
}
void print_menu_option(void)
{
    printf("-------------------------\n");
    printf("|Tapez le nombre :      |\n");
    printf("|1: Ajouter des joueur  |\n");
    printf("|2: Demarrer une partie |\n");
    printf("|3: Quitter             |\n");
    printf("-------------------------\n");
    printf("SELECTION : ");
}
void print_game_option(void)
{
    printf("---------------------------\n");
    printf("|Tapez le nombre :      |\n");
    printf("|1: Lancer les dés        |\n");
    printf("|2: Capturer un pokemon   |\n");
    printf("|3: Les propriétés        |\n");
    printf("|4: Fin de tour           |\n");
    printf("|5: Quitter               |\n");
    printf("---------------------------\n");
    printf("SELECTION : ");
}
void print_title(void)
{
    printf("%s\t------------------------------------\t\n", KRED);
    printf("\t|Bienvenu dans mon monopoly Pokemon|\t\n");
    printf("\t------------------------------------%s\t\n", KWHT);
}
void print_properties(card_t *map, player_t *players, size_t player_nbr)
{
    for (size_t i = 0; i < player_nbr; i++)
    {
        printf("Pokemon de : %s", players[i].name);
        for (size_t y = 0; y < 40; y++)
            if (map[y].owner && !strcmp(map[y].owner->name, players[i].name))
                printf("- %s avec %ld attaque et %ld objets\n", map[y].name, map[y].move, map[y].item);
    }
}
void clear_screen(void)
{
    printf("\E[H\E[2J");
}

void print_menu(void)
{
    char *input = NULL;
    size_t i = 0;
    size_t player_nbr = 0;
    player_t *players = NULL;
    card_t map[] = {
        {"DEPART", {0}, NULL, 0, 0, START, NONE},
        {"kranidos", {60, 2, 10, 30, 90, 160, 250, 30}, NULL, 0, 0, CITY, PIERRICK},
        {"Caisse de communauté", {0}, NULL, 0, 0, COMMUNITY, NONE},
        {"Onix", {60, 4, 20, 60, 180, 320, 450, 30}, NULL, 0, 0, CITY, PIERRICK},
        {"Vous perdez un combat", {200}, NULL, 0, 0, TAX, NONE},
        {"Centre pokemon NORD", {200}, NULL, 0, 0, CITY, NONE},
        {"Ceriflor", {100, 6, 30, 90, 270, 400, 550, 50}, NULL, 0, 0, CITY, FLO},
        {"Chance", {0}, NULL, 0, 0, LUCK, NONE},
        {"Tortipouss", {100, 6, 30, 90, 270, 400, 550, 50}, NULL, 0, 0, CITY, FLO},
        {"Roserade", {120, 8, 40, 100, 300, 450, 600, 60}, NULL, 0, 0, CITY, FLO},
        {"Prison Team Galaxy", {0}, NULL, 0, 0, PRISON, NONE},
        //-----------------Ligne 1----------------------

        {"Méditikka", {140, 10, 50, 150, 450, 625, 750, 70}, NULL, 0, 0, CITY, MELINA},
        {"Vous perdez un combat", {150}, NULL, 0, 0, TAX, NONE},
        {"Machopeur", {140, 10, 50, 150, 450, 625, 750, 70}, NULL, 0, 0, CITY, MELINA},
        {"Lucario", {160, 12, 60, 180, 500, 700, 900, 80}, NULL, 0, 0, CITY, MELINA},
        {"Centre pokemon EST", {200}, NULL, 0, 0, CITY, NONE},
        {"Léviator", {180, 14, 70, 200, 550, 750, 950, 90}, NULL, 0, 0, CITY, LOVIS},
        {"Caisse de communauté", {0}, NULL, 0, 0, COMMUNITY, NONE},
        {"Maraiste", {180, 14, 70, 200, 550, 750, 950, 90}, NULL, 0, 0, CITY, LOVIS},
        {"Mustéflott", {200, 16, 80, 220, 600, 800, 1000, 100}, NULL, 0, 0, CITY, LOVIS},
        {"Aire de détente", {0}, NULL, 0, 0, START, NONE},
        //-----------------Ligne 2----------------------

        {"Grodrive", {220, 18, 90, 250, 700, 875, 1050, 110}, NULL, 0, 0, CITY, KIMERA},
        {"Chance", {0}, NULL, 0, 0, LUCK, NONE},
        {"Ectoplasma", {220, 18, 90, 250, 700, 875, 1050, 110}, NULL, 0, 0, CITY, KIMERA},
        {"Magirêve", {240, 20, 100, 300, 750, 925, 1100, 120}, NULL, 0, 0, CITY, KIMERA},
        {"Centre pokemon SUD", {200}, NULL, 0, 0, CITY, NONE},
        {"Magnéton", {260, 22, 110, 330, 800, 975, 1150, 130}, NULL, 0, 0, CITY, CHARLES},
        {"Steelix", {260, 22, 110, 330, 800, 975, 1150, 130}, NULL, 0, 0, CITY, CHARLES},
        {"Vous perdez un combat", {150}, NULL, 0, 0, TAX, NONE},
        {"Bastiodon", {280, 22, 120, 360, 850, 1025, 1200, 140}, NULL, 0, 0, CITY, CHARLES},
        {"Capturer par la team Galaxy", {0}, NULL, 0, 0, START, NONE},
        //-----------------Ligne 3----------------------

        {"Cochignon", {300, 26, 120, 390, 850, 1025, 1200, 140}, NULL, 0, 0, CITY, GLADYS},
        {"Blizzaroi", {300, 26, 130, 390, 900, 1100, 1275, 150}, NULL, 0, 0, CITY, GLADYS},
        {"Caisse de communauté", {0}, NULL, 0, 0, COMMUNITY, NONE},
        {"Momartik", {320, 28, 150, 450, 1000, 1200, 1400, 160}, NULL, 0, 0, CITY, GLADYS},
        {"Centre pokemon OUEST", {200}, NULL, 0, 0, CITY, NONE},
        {"Chance", {0}, NULL, 0, 0, LUCK, NONE},
        {"Luxray", {350, 35, 175, 500, 1100, 1300, 1500, 175}, NULL, 0, 0, CITY, TANGY},
        {"Vous perdez un combat", {100}, NULL, 0, 0, TAX, NONE},
        {"Élekable", {400, 50, 200, 600, 1400, 1700, 2000, 200}, NULL, 0, 0, CITY, TANGY},
        //-----------------Ligne 5----------------------
    };

    print_menu_option();
    while (getline(&input, &i, stdin) != -1)
    {
        i = atoi(input);
        clear_screen();
        if (i == 1)
        {
            players = add_player(players, &player_nbr);
            clear_screen();
            player_list(players, player_nbr);
        }
        else if (i == 2)
        {
            if (player_nbr >= 2)
            {
                game_loop(players, player_nbr, map);
                clear_screen();
            }
            else
                printf("Ajoutez au moins 2 joueurs\n");
        }
        else if (i == 3)
        {
            clear_screen();
            free(input);
            input = NULL;
            printf("Au revoir!\n");
            break;
        }
        else
            printf("Mauvaise entré\n");
        print_menu_option();
    }
    free(input);
    destroy_players(&players, player_nbr);
}
