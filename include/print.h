#pragma once

void print_menu_option(void);
void print_title(void);
void print_map(void);
void print_players_stats(card_t *map, player_t *players, size_t player_nbr);
void print_menu(void);
void print_game_option(void);
void print_properties(card_t *map, player_t *players, size_t player_nbr);