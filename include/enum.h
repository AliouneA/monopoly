#pragma once

typedef enum TYPE
{
    LUCK = 0,
    COMMUNITY,
    TAX,
    CITY,
    PRISON,
    GOTO_PRISON,
    START
} TYPE;
typedef enum STATUS
{
    FREE = 0,
    JAIL
} STATUS;
typedef enum ARENE
{
    PIERRICK = 0,
    FLO,
    MELINA,
    LOVIS,
    KIMERA,
    CHARLES,
    GLADYS,
    TANGY,
    NONE
} ARENE;

typedef enum PRICE
{
    BASE = 0,
    RENT,
    FIRST_HOUSE,
    SECOUND_HOUSE,
    THIRD_HOUSE,
    FOURTH_HOUSE,
    HOTEL,
    MORTGAGE
} PRICE;

typedef enum STAGE
{
    GAME,
    MENU,
    PAUSE
} STAGE;