#include <stddef.h>
#pragma once

int coin_flip_start(size_t player_nbr);
int throw_dice(void);
