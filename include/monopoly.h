#include "enum.h"
#pragma once

typedef struct card_s
{
    char *name;
    int price[8];
    struct player_s *owner;
    size_t item;
    size_t move;
    TYPE type;
    ARENE arene;
} card_t;
typedef struct player_s
{
    STATUS status;
    char *name;
    int money;
    int position;
} player_t;
typedef struct bank_s
{
    size_t move;
    size_t item;
    int money;
} bank_t;

void game_loop(player_t *players, size_t player_nbr, card_t *map);