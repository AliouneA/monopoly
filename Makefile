SRC			=	src/main.c		\
				src/destroy.c	\
				src/print.c		\
				src/throw_dice.c\
				src/player.c	\
				src/game_loop.c

OBJ			=	$(SRC:.c=.o)

CFLAGS		= 	-Wall -Wextra -g3

CPPFLAGS	+=	-I ./include

NAME		=	monopoly

all:		$(NAME)

$(NAME):	$(OBJ)
			$(CC) $(OBJ) -o $(NAME)

clean:	
			$(RM) a.out
			$(RM) src/*.o

fclean: 	clean
			$(RM) $(NAME)

re: 		fclean all

.PHONY: 	all clean fclean re
